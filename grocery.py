import sqlite3
import random

con = sqlite3.connect("recipes.db")
cur = con.cursor()
category = []
reclist = {}
ingredients = []
finallist = {}
recnam = []
finalserv = 0


def addtolist(numday, people, meal, recnum):
    # This function searches through a database of recipes and generates a list of
    # ingredients

    global finalserv
    recsearch = []
    j = 0
    servings = 0

    # Checks the meal type and generates a list of recipes
    if meal == "breakfast":
        recamount = cur.execute(
            "SELECT recnamid FROM recipe_name WHERE mealtimeid == 1"
        ).fetchall()
        totalservings = numday * people
    else:
        recamount = cur.execute(
            "SELECT recnamid FROM recipe_name WHERE mealtimeid != 1"
        ).fetchall()
        totalservings = numday * people * 2

    finalserv = finalserv + totalservings

    for x in recamount:
        recsearch.append(x[0])
    print(len(recsearch))

    # Creates a random list of recipes of no more than the recnum number of recipes
    while len(recsearch) > recnum:
        recrand = random.randint(0, len(recsearch) - 1)
        del recsearch[recrand]

    random.shuffle(recsearch)

    # Loops through recipes to create a list of ingredients
    while servings < totalservings:

        recipe = cur.execute(
            "SELECT * FROM recipe_name WHERE recnamid = (?)", [recsearch[j]]
        ).fetchone()

        servings += int(recipe[4])
        if recipe[1] not in reclist:
            reclist[recipe[1]] = 1
        else:
            reclist[recipe[1]] += 1

        ingredients = cur.execute(
            "SELECT recipes.amount, measurements.measnam, ingredients.ingrednam, ingredients.prodid FROM recipes INNER JOIN ingredients ON recipes.ingredid = ingredients.ingredid INNER JOIN measurements ON recipes.measid = measurements.measid WHERE recipes.recnamid = (?)",
            [recsearch[j]],
        ).fetchall()

        # Loops through ingredients and adds them to a list. Ingredient amounts are added to the
        # total and unit conversions are performed if required.
        for ingredient in ingredients:
            if ingredient[3] not in category:
                category.append(ingredient[3])

            if ingredient[2] not in finallist:
                finallist[ingredient[2]] = [ingredient[0], ingredient[1], ingredient[3]]
            elif finallist[ingredient[2]][1] == ingredient[1]:
                finallist[ingredient[2]][0] = (
                    finallist[ingredient[2]][0] + ingredient[0]
                )
            elif finallist[ingredient[2]][1] != ingredient[1]:
                if (
                    finallist[ingredient[2]][1] == "cup(s)"
                    and ingredient[1] == "tablespoon"
                ):
                    finallist[ingredient[2]][0] = (
                        finallist[ingredient[2]][0] + ingredient[0] / 16
                    )
                elif (
                    finallist[ingredient[2]][1] == "cup(s)"
                    and ingredient[1] == "teaspoon"
                ):
                    finallist[ingredient[2]][0] = (
                        finallist[ingredient[2]][0] + ingredient[0] / 48
                    )
                elif (
                    finallist[ingredient[2]][1] == "tablespoon"
                    and ingredient[1] == "teaspoon"
                ):
                    finallist[ingredient[2]][0] = (
                        finallist[ingredient[2]][0] + ingredient[0] / 3
                    )
                elif (
                    finallist[ingredient[2]][1] == "tablespoon"
                    and ingredient[1] == "cup(s)"
                ):
                    finallist[ingredient[2]][0] = (
                        finallist[ingredient[2]][0] / 16 + ingredient[0]
                    )
                    finallist[ingredient[2]][1] = "cup(s)"
                elif (
                    finallist[ingredient[2]][1] == "teaspoon"
                    and ingredient[1] == "tablespoon"
                ):
                    finallist[ingredient[2]][0] = (
                        finallist[ingredient[2]][0] / 3 + ingredient[0]
                    )
                    finallist[ingredient[2]][1] = "tablespoon"
                elif (
                    finallist[ingredient[2]][1] == "teaspoon"
                    and ingredient[1] == "cup(s)"
                ):
                    finallist[ingredient[2]][0] = (
                        finallist[ingredient[2]][0] / 48 + ingredient[0]
                    )
                    finallist[ingredient[2]][1] = "cup(s)"
                elif (
                    finallist[ingredient[2]][1] == "ounce(s)"
                    and ingredient[1] == "cup(s)"
                ):
                    finallist[ingredient[2]][0] = (
                        finallist[ingredient[2]][0] + ingredient[0] * 1.5
                    )
                elif (
                    finallist[ingredient[2]][1] == "cup(s)"
                    and ingredient[1] == "ounce(s)"
                ):
                    finallist[ingredient[2]][0] = (
                        finallist[ingredient[2]][0] * 1.5 + ingredient[0]
                    )
                    finallist[ingredient[2]][1] = "ounce(s)"
                elif finallist[ingredient[2]][1] == "n/a" and (
                    ingredient[1] == "ounce(s)"
                    or ingredient[1] == "cup(s)"
                    or ingredient[1] == "tablespoon"
                    or ingredient[1] == "teaspoon"
                ):
                    finallist[ingredient[2]][0] = (
                        finallist[ingredient[2]][0] + ingredient[0]
                    )
                    finallist[ingredient[2]][1] = ingredient[1]
                elif (
                    (
                        finallist[ingredient[2]][1] == "ounce(s)"
                        or finallist[ingredient[2]][1] == "cup(s)"
                        or finallist[ingredient[2]][1] == "tablespoon"
                        or finallist[ingredient[2]][1] == "teaspoon"
                    )
                    and ingredient[1] == "n/a"
                    and ingredient[0] == 0
                ):
                    print("n/a")
                    print(ingredient)
                else:
                    print("unit error")
                    print(ingredient)
        if j == len(recsearch) - 1:
            j = 0
        else:
            j += 1

        recnam.append(recsearch)


# addtolist(7, 2, "breakfast", 1)
addtolist(4, 2, "dinner", 3)

# Creates a text file containing a count of the recipes, a list of the recipe names and
# the number of times they are used, and a list of ingredients orderd by area in the
# grocery store
x = 0
with open("grocerylist.txt", "w", encoding="utf-8") as f:
    f.write("Recipes (Servings: " + str(finalserv) + ")\n")
    for recipe, count in reclist.items():
        x += 1
        f.write(str(x) + ". " + recipe + " x " + str(count) + "\n")
    f.write("\n")

    for cat in sorted(category):
        area = cur.execute(
            "SELECT prodnam FROM producetype WHERE prodid = (?)", [cat]
        ).fetchone()
        area = area[0].title()
        f.write(area + "\n")
        for x, y in sorted(finallist.items()):
            amount = str(round(y[0], 2))
            unit = str(y[1])
            if finallist[x][2] == cat:
                if y[0] == 0:
                    f.write("\u2610 " + x + "\n")
                elif unit != "n/a":
                    f.write("\u2610 " + x + " (" + amount + " " + unit + ")" + "\n")
                else:
                    f.write("\u2610 " + x + " (" + amount + ")" + "\n")
        f.write("\n")
